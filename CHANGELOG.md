
# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]

### changed

### added

- new GDP and Population scenarios (COVID shock, SSP2Ariadne, SDP variants)
 - new FAO_online data

### removed

### fixed

## [4.63] - 12-08-2021

Current revision at time of changelog creation

[Preprocessing Respository]: https://gitlab.pik-potsdam.de/landuse/preprocessing-magpie/-/tree/main
